MACE
-------------
Miscreated Advanced Crafting Enhaced

ModID: 2480835664

Little tweaked crafting...

All components in one subcategory.

Components removed from others subcategories.

---------------------------------
Sleeping Bag - Placeable Respawner

Both Waterskins moved from Consumables - Water to Equipment - Bags

Medical Cannabis in Equipment - Medical

Hidden Stash in Base Building - Storage

Baking in Consumables - Food - Baking need Baking Guide

Repair Kit (Leather Items) in Equipment - Tools

---------------------------------
Lighter stack changed to 255

Matches stack changed to 50

DuctTape stack changed to 25

amcoinLedger stack changed to 10000

(show max 255 pieces but counting all...)

---------------------------------
Also added 2 recipes

Iron Ingot from 2 Scrap Metal
Leather repair kit from 2 WolfSkin

And Baking Guide
________________________________________________
![MACE](https://gitlab.com/miscreated/MAC/-/raw/MAC/MAC.jpg)
________________________________________________
Medical Cannabis thanks to:
https://github.com/Bealze/Gamer-Love

Hidden Stash and Sleeping Bag thanks to:
https://steamcommunity.com/id/PitiViers

Steam Mod Link:
https://steamcommunity.com/sharedfiles/filedetails/?id=2480835664

Feel free to contribute to project:
https://gitlab.com/miscreated/MACE

Tweak It! Share it! Enjoy it!
